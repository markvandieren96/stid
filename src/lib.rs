#[cfg(feature = "nanoserde_ron")]
use nanoserde::{DeRon, SerRon};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(
	feature = "serde_support",
	derive(serde::Serialize, serde::Deserialize)
)]
#[cfg_attr(feature = "nanoserde_ron", derive(nanoserde::SerRon, nanoserde::DeRon))]
pub struct Id(u32);

impl Id {
	pub const fn new(s: &str) -> Self {
		let id = const_fnv1a_hash::fnv1a_hash_32(s.as_bytes(), None);
		Self(id)
	}
}

impl std::fmt::Display for Id {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		self.0.fmt(f)
	}
}

/// Stable type id
pub trait Stid {
	const NAME: &'static str;
	const ID: Id;
}

/// Implement the stable type id trait for the given type, optionally using a custom name
#[macro_export]
macro_rules! stid {
	($ty:ty, $name:expr) => {
		impl $crate::Stid for $ty {
			const NAME: &'static str = $name;
			const ID: $crate::Id = $crate::Id::new($name);
		}
	};
	($ty:ty) => {
		$crate::stid!($ty, stringify!($ty));
	};
}

stid!((), "unit");
stid!(bool);
stid!(char);
stid!(i8);
stid!(i16);
stid!(i32);
stid!(i64);
stid!(i128);
stid!(isize);
stid!(u8);
stid!(u16);
stid!(u32);
stid!(u64);
stid!(u128);
stid!(usize);
stid!(f32);
stid!(f64);
stid!(String);

#[cfg(feature = "specs_types")]
mod specs_types {
	use specs::{Entity, World};
	stid!(Entity);
	stid!(World);
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_stid_uniqueness() {
		let mut ids = vec![
			<()>::ID,
			<bool>::ID,
			i8::ID,
			i16::ID,
			i32::ID,
			i64::ID,
			i128::ID,
			isize::ID,
			u8::ID,
			u16::ID,
			u32::ID,
			u64::ID,
			u128::ID,
			usize::ID,
			f32::ID,
			f64::ID,
			String::ID,
		];

		#[cfg(feature = "specs_types")]
		{
			ids.push(specs::Entity::ID);
			ids.push(specs::World::ID);
		}

		let num_ids_before = ids.len();
		ids.sort();
		ids.dedup();
		assert_eq!(num_ids_before, ids.len());
	}

	#[cfg(feature = "nanoserde_ron")]
	#[test]
	fn test_stid_nanoserde() {
		let id: Id = String::ID;
		let s = id.serialize_ron();
		let out = nanoserde::DeRon::deserialize_ron(&s).unwrap();
		assert_eq!(id, out);
	}
}
